/**
 * Created by hanni on 11/28/16.
 */
var parse = require('co-body');

var posts = [
    {
        _id: 1488,
        title:'Baaaakaa',
        text:'baka ne kot',
        author:'kot'
    },
    {
        _id: 1377,
        title:'Kooooot',
        text:'Kot ne baka',
        author:'Baka'
    },
    {
        _id: 48991,
        title:'Debili',
        text:'Pezdos vi rebzia',
        author:'Pidr kakoita'
    },
    ];

exports.getAllPosts = function *getAllPosts() {
    // console.log(this);
    // this.body = yield.throw(404, "hello");

    var self = this;
    self.body = JSON.stringify(posts);
};

exports.addNewPost = function *addNewPost() {
    var self = this;
    var post = yield parse(self);
    if(!post.title) {
        self.body = {
            error: true,
            message: 'Title is mandatory',
            status: 501
        };
        return null;
    }
    if(!post.text) {
        self.body = {
            error: true,
            message: 'Text is mandatory',
            status: 501
        };
        return null;
    }
    if(!post.author) {
        self.body = {
            error: true,
            message: 'Author is mandatory',
            status: 501
        };
        return null;
    }

    post._id = Date.now();
    posts.push(post);

    self.body = 'Added';
};

exports.updatePost = function *updatePost(id) {
    var self = this;
    var post = yield parse(self);

    if(!post.title) {
        self.body = {
            error: true,
            message: 'Title is mandatory',
            status: 501
        };
        return null;
    }
    if(!post.text) {
        self.body = {
            error: true,
            message: 'Text is mandatory',
            status: 501
        };
        return null;
    }
    if(!post.author) {
        self.body = {
            error: true,
            message: 'Author is mandatory',
            status: 501
        };
        return null;
    }

    var index = null;

    posts.forEach( function (value, i) {
        if(value._id == id) index = i;
    });

    if (index || index == 0) {
        posts[index].author = post.author;
        posts[index].title = post.title;
        posts[index].text = post.text;
        self.body = 'Updated!';
    } else {
        self.body = {
            error: true,
            message: 'Not Found: ' + id,
            status: 404
        };
    }
};

exports.getPostById = function *getPostById(id) {
    var self = this;
    var post = null;
    posts.forEach( function (value) {
        if(value._id == id) post = value;
    });

    console.log(post);

    if(post) {
        self.body = JSON.stringify(post);
    } else {
        self.body = {
            error: true,
            message: 'Not Found: ' + id,
            status: 404
        };
    }
};

exports.deletePost = function *deletePost(id) {
    var self = this;
    var index = null;

    posts.forEach( function (value, i) {
        console.log(value._id);

        if (value._id == id) index = i;
    });

    if (index || index == 0) {
        posts.splice(index, 1);
        self.body = 'Deleted';
    } else {
        self.body = {
            error: true,
            message: 'Not Found: ' + id,
            status: 404
        };
    }

};