/**
 * Created by hanni on 11/28/16.
 */
var koa = require('koa');
var app = koa();

require('./routes.js').routes(app);

app.listen(3333);
console.log('Port 3333');