/**
 * Created by hanni on 11/28/16.
 */
var route = require('koa-route');


exports.routes = function routes(app) {

    var posts = require('./components/posts');
    app.use(route.get('/', posts.getAllPosts));
    app.use(route.get('/:id', posts.getPostById));
    app.use(route.post('/', posts.addNewPost));
    app.use(route.put('/:id', posts.updatePost));
    app.use(route.del('/:id', posts.deletePost));

};